/**
 * Created with IntelliJ IDEA.
 * User: rpillai
 * Date: 31/05/13
 * Time: 10:48 AM
 * To change this template use File | Settings | File Templates.
 */
AJS.$('document').ready(function () {
    var isDeletePage = AJS.$('#comment-delete-submit');
    if (isDeletePage) {
        AJS.$('#comment-delete-submit').click(function () {
            AJS.$.getJSON(AJS.contextPath() + "/rest/handlecomments/latest/hdata/deletecomment?commentid=" + AJS.$('input[name="commentId"]').attr('value'), function (data) {
                console.log(data);
            });
        });
    }
});