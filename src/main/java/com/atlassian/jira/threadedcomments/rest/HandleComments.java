package com.atlassian.jira.threadedcomments.rest;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.comments.Comment;
import com.atlassian.jira.issue.comments.CommentManager;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.sal.api.transaction.TransactionCallback;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Hashtable;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * A resource of message.
 */
@Path("/hdata")
public class HandleComments {

    private static final Logger log = LogManager.getLogger("votecomments");
    private ActiveObjects ao;
    private IssueManager issueManager;
    private PermissionManager permissionManager;
    private CommentManager commentManager;

    public HandleComments(ActiveObjects ao, IssueManager issueManager, PermissionManager permissionManager, CommentManager commentManager) {
        this.ao = checkNotNull(ao);
        this.issueManager = issueManager;
        this.permissionManager = permissionManager;
        this.commentManager = commentManager;
    }

    @GET
    @AnonymousAllowed
    @Produces({MediaType.APPLICATION_JSON})
    @Path("commentdata")
    public Response commentData(@QueryParam("issueid") final Long issueid) {
        if (null == issueid) {
            return Response.notModified("Issue Id missing").build();
        } else {
            log.debug("Issueid - " + issueid);
        }
        final User loggedInUser = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser();
        final MutableIssue issueObject = issueManager.getIssueObject(issueid);
        final Hashtable<Integer, CommentModel> commentData = new Hashtable<Integer, CommentModel>();
        if (null != issueObject && permissionManager.hasPermission(Permissions.BROWSE, issueObject, loggedInUser)) {
            ao.executeInTransaction(new TransactionCallback<Void>() {
                @Override
                public Void doInTransaction() {
                    CommentInfo[] commentInfos = ao.find(CommentInfo.class, "ISSUE_ID = ?", issueid);
                    for (CommentInfo c : commentInfos) {
                        commentData.put(c.getID(), new CommentModel("", c.getParentCommentId(), c.getIssueId(), c.getCommentId()));
                    }
                    return null;
                }
            });

        } else {
            log.warn("Get comment request ignored");
        }
        return Response.ok(commentData.values()).build();
    }

    @POST
    @AnonymousAllowed
    @Produces({MediaType.APPLICATION_JSON})
    @Path("addcomment")
    public Response addComment(final CommentModel comment) {
        if (null == comment || (null == comment.getIssueId()) ||
                (null == comment.getParentCommentId()) ||
                (null == comment.getCommentBody())) {
            return Response.notModified("Required parameters missing").build();
        }

        final Comment commentObj = commentManager.getCommentById(comment.getParentCommentId());
        final User loggedInUser = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser();
        final MutableIssue issueObject = issueManager.getIssueObject(comment.getIssueId());

        if (null == commentObj) {
            return Response.notModified("Wrong parent comment id").build();
        } else if (!permissionManager.hasPermission(Permissions.COMMENT_ISSUE, issueObject, loggedInUser)) {
            return Response.notModified("No permissions to comment").build();
        }

        final Comment newComment = commentManager.create(issueManager.getIssueObject(comment.getIssueId()),
                ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser().getName(),
                StringEscapeUtils.unescapeHtml4(comment.getCommentBody().replaceAll("\\n", "\n")), true);
        log.debug(newComment.getId());
        ao.executeInTransaction(new TransactionCallback<Void>() {
            @Override
            public Void doInTransaction() {
                final CommentInfo commentInfo = ao.create(CommentInfo.class);
                commentInfo.setCommentId(newComment.getId());
                commentInfo.setParentCommentId(comment.getParentCommentId());
                commentInfo.setIssueId(comment.getIssueId());
                commentInfo.save();
                return null;
            }
        });
        comment.setCommentId(newComment.getId());

        return Response.ok(comment).build();
    }

    @GET
    @AnonymousAllowed
    @Produces({MediaType.APPLICATION_JSON})
    @Path("deletecomment")
    public Response deleteComment(@QueryParam("commentid") final Long commentid) {
        if (null == commentid) {
            return Response.notModified("commentid Id missing").build();
        } else {
            log.debug("commentid - " + commentid);
        }
        final Comment commentObj = commentManager.getCommentById(commentid);
        final User loggedInUser = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser();

        if (null == commentObj) {
            return Response.notModified("Wrong comment id").build();
        } else {
            final Issue issueObject = commentObj.getIssue();
            if (!loggedInUser.getName().equals(commentObj.getAuthorApplicationUser().getName()) &&
                    !permissionManager.hasPermission(Permissions.COMMENT_DELETE_ALL, issueObject, loggedInUser)) {
                return Response.notModified("Attempt to delete others comments, but no permission").build();
            } else if (!permissionManager.hasPermission(Permissions.COMMENT_DELETE_OWN, issueObject, loggedInUser)) {
                return Response.notModified("No permissions to delete own comment").build();
            }
        }
        //Find the current comments parent
        final Long[] parentComment = {new Long(0)};
        ao.executeInTransaction(new TransactionCallback<Void>() {
            @Override
            public Void doInTransaction() {
                CommentInfo[] commentInfos = ao.find(CommentInfo.class, "COMMENT_ID = ?", commentid);
                if (commentInfos.length > 1) {
                    log.error("This is not good, one comment can have only one parent. They are not humans");
                }
                if (commentInfos.length > 0) {
                    parentComment[0] = commentInfos[0].getParentCommentId();
                }
                return null;
            }
        });
        //Migrate the the existing children to the parent of the comment getting deleted
        ao.executeInTransaction(new TransactionCallback<Void>() {
            @Override
            public Void doInTransaction() {
                CommentInfo[] commentInfos = ao.find(CommentInfo.class, "PARENT_COMMENT_ID = ?", commentid);
                for (CommentInfo c : commentInfos) {
                    if (parentComment[0] != 0) {
                        c.setParentCommentId(parentComment[0]);
                    } else {
                        //Remove the records which has this comment as the parent and no migration to the done
                        ao.delete(c);
                    }
                    c.save();
                }
                return null;
            }
        });

        return Response.ok("Deletion done").build();
    }
}